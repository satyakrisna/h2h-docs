package com.h2h.docs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name="T_PROJECT")
public class Project {
	
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID", columnDefinition = "VARCHAR(36)", unique = true, nullable = false)
    private String id;
	
	@Column(name = "NAME", columnDefinition = "VARCHAR(40)", unique = false, nullable = false)
    private String name;
	
	@Column(name = "DESCRIPTION", columnDefinition = "VARCHAR(255)", unique = false, nullable = false)
    private String description;
	
	@Column(name = "PIC", columnDefinition = "VARCHAR(40)", unique = false, nullable = false)
    private String pic;
	
}

package com.h2h.docs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.h2h.docs.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String>{

}

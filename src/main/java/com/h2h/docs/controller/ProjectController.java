package com.h2h.docs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.h2h.docs.entity.Project;
import com.h2h.docs.service.ProjectService;

@RestController
@RequestMapping("/api")
public class ProjectController {

	@Autowired
	ProjectService projectService;
	
	@GetMapping("/project")
	public List<Project> findAllProject(){
		return projectService.findAllProject();
	}
	
	@GetMapping("/project-by-id")
	public Project findProjectById(@RequestParam String projectId){
		return projectService.findProjectById(projectId);
	}
	
	@PostMapping("/project-create")
	public Project createProject(@RequestBody Project project){
		return projectService.createProject(project.getName(), project.getDescription(), project.getPic());
	}
	
	@PostMapping("/project-update")
	public Project updateProject(@RequestBody Project project){
		return projectService.updateProject(project.getId(), project.getName(), project.getDescription(), project.getPic());
	}
	
	@PostMapping("/project-delete")
	public void deleteProject(@RequestParam String projectId){
		projectService.deleteProject(projectId);
	}
}

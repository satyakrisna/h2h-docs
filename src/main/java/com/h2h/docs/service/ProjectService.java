package com.h2h.docs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.h2h.docs.entity.Project;
import com.h2h.docs.repository.ProjectRepository;

@Service
public class ProjectService {

	@Autowired
	ProjectRepository projectRepository;

	// Read all
	@Transactional
	public List<Project> findAllProject() {
		List<Project> projectList = new ArrayList<Project>();
		try {
			projectList = projectRepository.findAll();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectList;
	}

	// Read by id
	@Transactional
	public Project findProjectById(String projectId) {
		Project project = new Project();
		try {
			Optional<Project> projectOptinal = projectRepository.findById(projectId);
			project = projectOptinal.get();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return project;
	}

	// Create
	@Transactional
	public Project createProject(String name, String description, String pic) {
		Project project = new Project();
		try {
			project.setName(name);
			project.setDescription(description);
			project.setPic(pic);
			projectRepository.save(project);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return project;
	}

	// Update
	@Transactional
	public Project updateProject(String projectId, String name, String description, String pic) {
		Project project = new Project();
		try {
			Optional<Project> projectOptinal = projectRepository.findById(projectId);
			project = projectOptinal.get();

			if (null != name || !"".equalsIgnoreCase(name)) {
				project.setName(name);
			}

			if (null != description || !"".equalsIgnoreCase(description)) {
				project.setDescription(description);
			}

			if (null != pic || !"".equalsIgnoreCase(pic)) {
				project.setPic(pic);
			}
			projectRepository.save(project);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return project;
	}

	// Delete
	@Transactional
	public void deleteProject(String projectId) {
		try {
			projectRepository.deleteById(projectId);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
